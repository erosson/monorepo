def serve(srcs, **kwargs):
  """Launch a pushState-capable HTTP server for static files in a directory."""
  native.sh_binary(
    srcs=["//tools:serve.sh"],
    data=["@bazel_tools//tools/bash/runfiles", "//tools:serve"] + srcs,
    args=["$(location " + srcs[0] + ")"],
    **kwargs)

def netlify_deploy(site, dir, functions=None, **kwargs):
  """Manual deploy to netlify"""
  native.sh_binary(
    srcs=["//tools:netlify-deploy.sh"],
    data=["@bazel_tools//tools/bash/runfiles", "//tools:netlify"] + [dir] + ([functions] if functions != None else []),
    args=[site, "--dir=$(location " + dir + ")"] + (["--functions=$(location " + functions + ")"] if functions != None else []),
    **kwargs)
