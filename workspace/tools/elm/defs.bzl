load("@build_bazel_rules_nodejs//:defs.bzl", "nodejs_binary", "nodejs_test")

def _elm_test_impl(ctx):
  content = r"""#!/bin/bash -eu
    ELM_TEST=`realpath {elm_test}`
    ELM=`realpath {elm}`
    # elm needs HOME defined, and `bazel test` removes it.
    export HOME=${{HOME:-${{TEST_TMPDIR:-}}}}
    # nuke cached data before tests - it sometimes breaks the compiler if TEST_TMPDIR has changed.
    rm -rf {dirname}/elm-stuff

    cd {dirname}
    $ELM_TEST --compiler=$ELM
    """.format(
      dirname=ctx.file.json.dirname,
      elm_test=ctx.files._elm_test[0].short_path,
      elm=ctx.files._elm[0].short_path,
    )
  ctx.actions.write(ctx.outputs.executable, content, is_executable=True)
  return struct(
    runfiles = ctx.runfiles(ctx.files._elm_test + ctx.files.srcs + ctx.files.tests + [ctx.file.json])
      .merge(ctx.attr._elm_test.default_runfiles)
      .merge(ctx.attr._elm.default_runfiles)
  )
_elm_test = rule(
  test = True,
  attrs = {
    "json": attr.label(mandatory=True, allow_single_file=True),
    "srcs": attr.label_list(mandatory=True, allow_files=True),
    "tests": attr.label_list(mandatory=True, allow_files=True),
    "_elm_test": attr.label(default="@npm//elm-test/bin:elm-test", executable=True, cfg="host", allow_files=True),
    "_elm": attr.label(default="@npm//elm/bin:elm", executable=True, cfg="host", allow_files=True),
  },
  implementation = _elm_test_impl,
)
def elm_test(json="elm.json", srcs=None, tests=None, **kwargs):
  _elm_test(
    json=json,
    srcs=srcs if srcs else elm_app_srcs(json=False),
    tests=tests if tests else elm_app_tests(),
    **kwargs)


def elm_app_binary(json="elm.json", srcs=[], args=[], **kwargs):
  # not sure what's broken here... a rule similar to elm_app_test can't find
  # bash runfiles, no matter what I've tried. sh_binary macro works though.
  #_elm_app_binary(
  #  json=json,
  #  srcs=srcs if srcs else elm_app_srcs(json=False),
  #  **kwargs)
  srcs=srcs if srcs else elm_app_srcs(json=False)
  native.sh_binary(
    srcs=["//tools/elm:elm-app.sh"],
    data=["@bazel_tools//tools/bash/runfiles", "//tools:elm-app"] + [json] + srcs,
    args=[native.package_name()] + args,
    **kwargs)


def _elm_app_test_impl(ctx):
  elm_app_template = r"""#!/bin/bash -eu
ELM_APP=`realpath {elm_app}`
ELM=`realpath {elm}`
# elm needs HOME defined, and `bazel test` removes it.
export HOME=${{HOME:-${{TEST_TMPDIR:-}}}}
# nuke cached data before tests - it sometimes breaks the compiler if TEST_TMPDIR has changed.
rm -rf {dirname}/elm-stuff

cd {dirname}
../../{elm_app} {args}
"""
  runner_content = elm_app_template.format(
    dirname=ctx.file.json.dirname,
    elm_app=ctx.files._elm_app[0].short_path,
    elm=ctx.files._elm[0].short_path,
    bash_runfiles=ctx.file._bash_runfiles.short_path,
    args=' '.join(["test"] + ctx.attr.args),
  )
  ctx.actions.write(
    content=runner_content,
    output=ctx.outputs.executable,
  )
  return struct(
    runfiles = ctx.runfiles(
      files=ctx.files._elm_app + ctx.files.srcs + ctx.files.tests + [ctx.file.json],
      )
      .merge(ctx.attr._elm_app.default_runfiles)
      .merge(ctx.attr._elm.default_runfiles)
      .merge(ctx.attr._bash_runfiles.default_runfiles)
  )
_elm_app_test = rule(
  test = True,
  attrs = {
    "json": attr.label(mandatory=True, allow_single_file=True),
    "srcs": attr.label_list(mandatory=True, allow_files=True),
    "tests": attr.label_list(mandatory=True, allow_files=True),
    "_elm_app": attr.label(default="@npm//create-elm-app/bin:elm-app", executable=True, cfg="host", allow_files=True),
    "_elm": attr.label(default="@npm//elm/bin:elm", executable=True, cfg="host", allow_files=True),
    "_bash_runfiles": attr.label(default="@bazel_tools//tools/bash/runfiles", allow_single_file=True)
  },
  implementation = _elm_app_test_impl,
)
def elm_app_test(json="elm.json", srcs=None, tests=None, **kwargs):
  _elm_app_test(
    json=json,
    srcs=srcs if srcs else elm_app_srcs(json=False),
    tests=tests if tests else elm_app_tests(),
    **kwargs)

def elm_app_srcs(json=True):
  return native.glob((["elm.json"] if json else []) + ["src/**/*", "public/**/*"])

def elm_app_tests():
  return native.glob(["tests/**/*"])

def elm_app_build(srcs=None, outs=None, **kwargs):
  # TODO: can't seem to use elm-app-binary in build rules. runfiles "should" be usable if srcs, listed in tools. Why?
  # Instead, we'll redo its directory shenanigans here.
  native.genrule(
    srcs=srcs if srcs else elm_app_srcs(),
    outs=outs if outs else ["build"],
    tools=["//tools:elm-app"],
    cmd="""#!/bin/sh -eux
    DIR="{package}"
    # change foo/bar/baz to ../../..
    UNDIR=`echo {package} | sed -r 's/[^\/]+/../g'`
    # location returns a directory relative to the workspace root, but elm-app requires running from the package root
    (cd $$DIR && HOME=$@D $$UNDIR/$(location //tools:elm-app) build)
    mv $$DIR/build $(location build)
    """.format(package=native.package_name()),
    **kwargs)

load("//tools:defs.bzl", "serve")
def elm_app_project(outs=None):
  outs=outs if outs else ["build"]
  elm_app_binary(name="start", args=["start"])
  elm_app_test(name="test")
  elm_app_build(name="build-rule", outs=outs)
  serve(name="start-build", srcs=outs)
