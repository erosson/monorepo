Elm and create-elm-app build rules for Bazel.

Elm-test usage:

    # BUILD
    load("//tools/elm:defs.bzl", "elm_test")
    elm_test(name="test", size="small")

Elm-app usage:

    # BUILD
    load("//tools/elm:defs.bzl", "elm_app_rules")
    # defines start, test, build, and start-build cmds
    elm_app_rules()
