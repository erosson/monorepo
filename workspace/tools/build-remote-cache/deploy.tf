terraform {
  backend "s3" {
    bucket = "terraform-backend.erosson.org"
    key    = "build-remote-cache"
    region = "us-east-1"
  }
}

provider "google" {
  version = "~> 1.20"
  project = "erosson-monorepo"
}

provider "gitlab" {
  # version: built locally from github
}

resource "google_project" "erosson_monorepo" {
  name       = "erosson-monorepo"
  project_id = "erosson-monorepo"
}

# https://console.cloud.google.com/storage/browser/erosson-monorepo-bazel-remote-cache
resource "google_storage_bucket" "bazel_remote_cache" {
  name     = "erosson-monorepo-bazel-remote-cache"
  location = "US"

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = 14
    }
  }
}

# status: https://console.cloud.google.com/iam-admin/iam?project=erosson-monorepo
resource "google_service_account" "ci_user" {
  account_id   = "ciuser"
  display_name = "ciuser"
}

resource "google_service_account_key" "ci_user" {
  service_account_id = "${google_service_account.ci_user.account_id}"

  #project="${google_project.erosson_monorepo.name}"
}

#data "google_service_account_key" "ci_user" {
#  name = "${google_service_account_key.ci_user.name}"
#  public_key_type = "TYPE_X509_PEM_FILE"
#}
# https://cloud.google.com/storage/docs/access-control/iam-roles
resource "google_storage_bucket_iam_member" "object_admin" {
  bucket = "${google_storage_bucket.bazel_remote_cache.name}"
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.ci_user.email}"
}

resource "gitlab_project" "monorepo" {
  name             = "monorepo"
  description      = "testing a public monorepo!"
  visibility_level = "public"
  default_branch   = "master"
}

# Currently (2018/12), I have to compile the gitlab terraform provider to get this one working. Worth the trouble.
# https://github.com/terraform-providers/terraform-provider-gitlab/issues/1
# https://github.com/terraform-providers/terraform-provider-gitlab/blob/master/gitlab/resource_gitlab_project_variable.go
resource "gitlab_project_variable" "remote_cache_auth" {
  project = "${gitlab_project.monorepo.id}"
  key     = "GOOGLE_CREDENTIALS_JSON_BODY"
  value   = "${base64decode(google_service_account_key.ci_user.private_key)}"

  #value="${google_service_account_key.ci_user.private_key}"
}

# for debugging
#output "auth" {
#  # https://github.com/terraform-providers/terraform-provider-google/issues/400#issuecomment-360565229
#  value="${base64decode(google_service_account_key.ci_user.private_key)}"
#}

