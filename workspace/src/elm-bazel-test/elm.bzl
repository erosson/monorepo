"""Elm build rules.
"""
# step 1: make the elm executable a binary target for bazel. download and extract the binary.
def _empty_impl(ctx):
  print("lolol", ctx)
  ctx.actions.run(
    executable="elm",
    outputs=["out"],
  )

empty = rule(
  implementation = _empty_impl,
)
